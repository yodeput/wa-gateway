FROM node:lts-alpine

WORKDIR /app
EXPOSE 3000

RUN set -x && yarn

COPY package.json yarn.lock ./

RUN touch .env

COPY . .

CMD [ "yarn", "start" ]
