import { Sequelize } from "sequelize";
import mysql from "mysql2";
import { modules } from "../../lib/index.js";
import { moment } from "./index.js";
import {Databases, Client} from "node-appwrite";


const { DB_NAME, DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_DIALECT } = process.env;

let connection = mysql.createPool({ waitForConnections: true, connectTimeout: 30000, host: DB_HOST, port: DB_PORT, user: DB_USER, password: DB_PASSWORD });
const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD, {
	host: DB_HOST,
	port: DB_PORT,
	dialect: DB_DIALECT,
	logging: false,
});

async function connectDatabase() {
	connection.query(`use \`${DB_NAME}\`;`, (err) => {
		if (err) {
			console.log(modules.color("[APP]", "#EB6112"), modules.color(moment().format("DD/MM/YY HH:mm:ss"), "#F8C471"), modules.color(`Create new Database Success!`, "#82E0AA"));
			connection.query(`CREATE DATABASE IF NOT EXISTS \`${DB_NAME}\`;`);
		}
	});
	await modules.sleep(2000);
	await sequelize
		.authenticate()
		.then(() => {
			console.log(modules.color("[APP]", "#EB6112"), modules.color(moment().format("DD/MM/YY HH:mm:ss"), "#F8C471"), modules.color(`Connection Database has been established Successfully`, "#82E0AA"));
		})
		.catch((error) => {
			console.error(error);
		});

	await sequelize.sync({ force: false, alter: false }).then(() => {
		console.log(modules.color("[APP]", "#EB6112"), modules.color(moment().format("DD/MM/YY HH:mm:ss"), "#F8C471"), modules.color(`Re-Sync Database`, "#82E0AA"));
	});
}
const appWrite = new Client();

appWrite
		.setEndpoint('https://appwrite.yogidputra.dev/v1')
		.setKey('04936ac2af6b9792e62453efbf7f90fba6033011984089433dfbb837381e639b79bff3a55ca5ecfe2f69513cc24892a3d0635631220bba848587de445ba4d3fb68fbffd1662e90af580009a50fba348eae87236368bc87903db84d8646840b9ddbaf1b75ab86407c4000629f01fdbedd2d62122a00038308cc4047eda9fc1529')
		.setProject('64102da289f75304c045');

const databases = new Databases(appWrite);

export {appWrite, databases, sequelize };
