import Session from "../models/session.model.js";
import {databases} from "../../config/Database.js";
import {ID, Query} from "node-appwrite";


class SessionDatabase {
	constructor() {
		this.session = Session;
	}

	async createSessionDB(session_name, session_number) {
		console.log(session_name, session_number);
		await databases.createDocument(
				'64102e652337a7937bea',
				'64102ee2481d68b26fbe',
				ID.unique(),
				{
					session_name, session_number, status: "CONNECTED"
				}
		);
	}

	async deleteSessionDB(session_name) {
		const sesi = await databases.listDocuments(
				'64102e652337a7937bea',
				'64102ee2481d68b26fbe',
				[
					Query.equal('session_name', session_name)
				]
		);

		if (sesi.total > 0) {
			await databases.deleteDocument(
					'64102e652337a7937bea',
					'64102ee2481d68b26fbe',
			sesi.documents[0].$id
			)
		}
	}

	async findOneSessionDB(session_name) {
		const sesi = await databases.listDocuments(
				'64102e652337a7937bea',
				'64102ee2481d68b26fbe',
				[
					Query.equal('session_name', session_name)
				]
		);
		if (sesi.total > 0) {
			return sesi.documents[0]
		} else {
			return false;
		}
	}

	async findAllSessionDB() {
		const list = await databases.listDocuments(
				'64102e652337a7937bea',
				'64102ee2481d68b26fbe',
		)
		if (list.total > 0) {
			return list.documents;
		}
		return [];
	}

	async updateStatusSessionDB(session_name, status) {
		const sesi = await databases.listDocuments(
				'64102e652337a7937bea',
				'64102ee2481d68b26fbe',
				[
					Query.equal('session_name', session_name)
				]
		);
		if (sesi.total > 0) {
			return await databases.updateDocument(
					'64102e652337a7937bea',
					'64102ee2481d68b26fbe',
					sesi.documents[0].$id,
					{
						status
					}
			)
		} else {
			return false;
		}
	}

	async startProgram() {
		const array = await databases.listDocuments(
				'64102e652337a7937bea',
				'64102ee2481d68b26fbe',
		);
		if (Array.isArray(array.documents) && array.total !== 0) {
			array.documents.map(async (value) => {
				value.status = "STOPPED";
				return await databases.updateDocument(
						'64102e652337a7937bea',
						'64102ee2481d68b26fbe',
						value.$id,
						{
							status: 'STOPPED'
						}
				)
			});
		}
	}
}

export default SessionDatabase;
