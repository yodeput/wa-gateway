import {moment} from "../../config/index.js";
import {databases} from "../../config/Database.js";
import {ID, Query} from "node-appwrite";

class ButtonResponse {
  constructor() {
  }

  async createButtonResponse(session_name, target_number, msg_id, button, btnMessage) {
    for (let j = 0; j < button.filter((x) => x != "").length; j++) {

      await databases.createDocument(
          '64102e652337a7937bea',
          '64102f043fcfeb305f3d',
          ID.unique(),
          {
            session_name,
            target_number,
            msg_id,
            keyword: button.filter((x) => x != "")[j],
            response: btnMessage.filter((x) => x != "")[j]
          }
      );
    }
  }

  async checkKeyword(keyword, target_number) {
    const array = await databases.listDocuments(
        '64102e652337a7937bea',
        '64102f043fcfeb305f3d',
        [
          Query.equal('keyword', keyword)
        ]
    );

    if (array.total > 0) {
      return array.documents[index];
    } else {
      return false;
    }
  }

  async deleteKeyword(msg_id, keyword) {
    const array = await databases.listDocuments(
        '64102e652337a7937bea',
        '64102f043fcfeb305f3d',
        [
          Query.equal('keyword', keyword)
        ]
    );

    if (array.total > 0) {
      await databases.deleteDocument(
          '64102e652337a7937bea',
          '64102f043fcfeb305f3d',
          array.documents[0].$id
      )
    } else {
      return false;
    }
  }
}

class ListResponse {
  constructor() {
  }

  async createListResponse(session_name, target_number, msg_id, list, responList) {
    for (let j = 0; j < button.filter((x) => x != "").length; j++) {

      await databases.createDocument(
          '64102e652337a7937bea',
          '64102eeca8b2c1f31fa6',
          ID.unique(),
          {
            session_name,
            target_number,
            msg_id,
            keyword: list.filter((x) => x != "")[j],
            response: responList.filter((x) => x != "")[j]
          }
      );
    }
  }

  async checkKeyword(keyword, target_number) {
    const array = await databases.listDocuments(
        '64102e652337a7937bea',
        '64102eeca8b2c1f31fa6',
        [
          Query.equal('keyword', keyword)
        ]
    );

    if (array.total > 0) {
      return array.documents[index];
    } else {
      return false;
    }
  }
}

class AutoReply {
  constructor() {
  }

  async createAutoReply(session_name, session_number, keyword, response) {

    let date = moment().format("DD/MM/YY HH:mm:ss");
    await databases.createDocument(
        '64102e652337a7937bea',
        '64102ef83b495e377ccf',
        ID.unique(),
        {session_name, session_number, keyword, date, response}
    );
  }

  async checkExistAutoReply(session_number, keyword) {
    const array = await databases.listDocuments(
        '64102e652337a7937bea',
        '64102ef83b495e377ccf',
        [
          Query.equal('session_number', session_number)
        ]
    );

    if (array.total > 0) {
      return true;
    } else {
      return false;
    }
  }

  async checkReplyMessage() {
    const array = await databases.listDocuments(
        '64102e652337a7937bea',
        '64102ef83b495e377ccf',
    );
    return array.documents;
  }

  async editReplyMessage(session_number, keyword, newKeyword, newRespon) {

    const array = await databases.listDocuments(
        '64102e652337a7937bea',
        '64102ef83b495e377ccf',
        [
          Query.equal('session_number', session_number),
          Query.equal('keyword', keyword)
        ]
    );

    if (array.total > 0) {
      array.documents.map(async (value) => {
        return await databases.updateDocument(
            '64102e652337a7937bea',
            '64102ef83b495e377ccf',
            array.documents[0].$id,
            {
              keyword: newKeyword, response: newRespon
            }
        )
      })
    } else {
      return false;
    }
  }

  async deleteReplyMessage(id) {
    await databases.deleteDocument(
        '64102e652337a7937bea',
        '64102ef83b495e377ccf',
        id
    )
  }

  async checkMessageUser(session_number, keyword) {
    return false;
  }

  async deleteAllKeyword() {
    return false;
  }
}

export {ButtonResponse, ListResponse, AutoReply};
