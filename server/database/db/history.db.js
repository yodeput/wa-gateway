import History from "../models/history.model.js";
import {moment} from "../../config/index.js";
import {appWrite, databases} from "../../config/Database.js";
import {ID} from "node-appwrite";

class HistoryMessage {
  constructor() {
    this.history = History;
  }

  async pushNewMessage(session_name, type, target, caption) {
    let date = moment().format("DD/MM/YY HH:mm:ss");
    await databases.createDocument(
        '64102e652337a7937bea',
        '64102ed9a04f562ee2e3',
        ID.unique(),
        {
          session_name,
          target,
          type,
          date,
          caption
        }
    );
  }

  async getAllMessage() {
		const list = await databases.listDocuments(
				'64102e652337a7937bea',
				'64102ed9a04f562ee2e3',
		)
		if (list.total > 0) {
			return list.documents;
		}
		return [];
  }

  async deleteHistory(id) {
		return await databases.deleteDocument(
				'64102e652337a7937bea',
				'64102ed9a04f562ee2e3',
				id
		)
  }
}

export default HistoryMessage;
